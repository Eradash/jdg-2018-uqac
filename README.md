# LISEZ-MOI (Fr)
English follows

## Repo de l'épreuve pratique

La correction de l'épreuve se fera avec la branche master de votre fork de ce repo.
Pour créer votre fork:  
1. Accédez à la page principale du repo de l'épreuve.  
2. À gauche de l'écran, appuyez sur l'icône "+" et cliquez sur le dernier choix en bas - "Fork this repository"  
3. Donnez un nom clair au fork qui nous permet d'identifier votre université (UQTR, ULaval, etc.)  
	**Assurez vous de laisser votre repo public (Ne pas cocher "This is a private repository")**

Git Bash/GUI est installé sur vos postes. Il est probable que le clone ssh ne fonctionne pas, il est donc recommandé que 
vous cloniez par https afin de vous assurer d'être en mesure de push vos modifications.

De manière générale, une liberté vous est laissée sur la convention des noms de fichiers et cie. Toutefois, vos solutions doivent
être séparées par numéros de la même manière que le repo vous est fourni. L'installation de programmes externes, de module python
et autre est permise. N'hésitez pas à ajouter un readme à un numéro si votre solution est particulière à comprendre
ou à éxecuter. S'il vous plait garder cela simple si possible.


## Environnement STM32F4

La manière la plus rapide pour développer et programmer le STM32F4 Discovery sur vos ordinateurs est d'utiliser Attolic TrueStudio.
L'environnement est vide. Pour créer un nouveau projet pour le STM32F4, suivez la procédure suivante:  
1. Dans File, cliquez sur New -> C Project  
2. Donnez un nom quelconque à votre project  
3. Décochez "Use default location" et pointez vers un dossier de votre fork du repo de l'épreuve.  
4. Dans l'encadré de gauche (Project type), sélectionnez Executable -> Embedded C Project, puis appuyez sur Next.  
5. Dans la case Target, entrez "STM32F4" et sélectionnez l'entrée STM32F4_Discovery (devrait être la première dans STMicroelectronics -> Board), puis appuyez sur Next.  
6. Laissez les options par défaut puis appuyez sur Next puis sur Finish.  

Pour valider que votre environnement fonctionne, branchez votre Discovery à l'ordinateur et appuyez sur l'icône debug. Quand le build
est terminé et que la vue change pour le mode debug, appuyez sur le bouton play. Les quatres leds du board devraient s'allumer.

Le point d'entré du programme se trouve dans le fichier main.c.

## Comment utiliser Python

À cause de comment l'ordinateur a été configurer par l'université, si vous essayez d'importer une librairie python en utilisant l'interpréteur par défaut ainsi que Pip, vous
allez frapper un "access denied" puisque vous n'avez pas un accès root.

Pour contourner cette limitation, vous pouvez utiliser Anaconda (qui est déjà installé sur votre poste) pour gérer vos différents environnements virtuels et installez des librairies pour ces
environnements.

Démarrez Anaconda en lançant "Anaconda Prompt".

Tapez "conda info --envs" pour lister les différents environnements disponibles. Normalement vous n'en verrez qu'un seul : "root".

Créez un nouvel environnement en tapant :

"conda create --name envName python=3.5" et tapez y lorsque demandé.

Copiez le chemin d'installation (dans cette exemple : C:\Users\jdg2018\AppData\Local\conda\conda\envs\envName)

Si vous tapez encore une fois "conda info --envs" vous devriez voir votre nouvel environnement.

Activez cet environnement en tapant "activate envName". Vous devriez voir (envName) en avant de votre ligne de commande.

Vous pouvez désormais installer toutes librairies que vous jugez nécessaires en utilisant pip.

Par exemple : "pip install see"

Vous pouvez lister les librairies installées dans votre environnement en tapant "conda list".

Pour plus d'informations sur comment utiliser Anaconda : https://conda.io/docs/user-guide/getting-started.html

Si vous souhaitez utiliser cet environnement dans Pycharm, vous devez cliquer sur "File->Settings->Project->Project Interpreter
-> L'engrenage gris ->Add local" et copier le chemin d'installation de votre librairies (que vous avez copier plus tôt) en avant de \python.exe.

Ensuite cliquez sur ok et apply. Maintenant l'interpréteur devrait utiliser votre environnement.

Rappelez vous de fournir un "readme" clair et de lister toutes les librairies que vous utilisez afin de faciliter la vie du correcteur. Assurez-vous qu'il puisse facilement exécuter vos différents projets.
Un correcteur heureux est un correcteur qui donne plus de points ;).

# README (En)

## Exam's repo

The correction of your work will be done with your fork of this repo.
to create a fork:  
1. Access this repo's main screen.  
2. On the left of your screen, press the "+" icon and select the last option - "Fork this repository"  
3. Give a clear name to your fork that allows us to easily identify your university (UQTR, ULaval, etc.)  
	**Make sure to leave your repo public (Untick "This is a private repository")**

Git Bash/GUI is installed on your computers. git over ssh probably won't work, so it is recommanded that you clone over
https to make sure you are able to push your modifications.

Generally, you are free to name your files as you please. However, your solutions must be separated using the folder structure
as present in this repo. Installation of external programs, python modules and other is permitted. If a certain solution is
harder to execute or understand, do not hesitate to add a readme file. Please keep is as simple as possible.

## STM32F4 Environment

The fastest way to program the STM32F4 Discovery kit on your computers is to use Atollic TrueStudio.
the abse environment is empty. To create a new project for the kit, do as follow:  
1. In File, click on New -> C Project  
2. Give a name to your project  
3. Untick "Use default location" and browse towards a folder in your fork of this exam's repo.  
4. In the left space (Project type), select Executable -> Embedded C Project, and then click on Next.  
5. As target, enter "STM32F4" and select STM32F4_Discovery (which should appear first in STMicroelectronics -> Board), and then click on Next.  
6. Leave options as is and then click on Next and then on Finish.  

In order to validate your environment, plug in your discovery kit and click on the debug icon. When the build is done and the view switches to 
debug mode, click on the play button. The board's four LEDs should light up.

The program entry point is in file main.c.

## How to use Python

Because of how the installation was made on the University's computer if you try to import package using pip with the default python 
you will get a denied access since you are not the root user.

To go around that, you can use Anaconda (which is already installed) to manage your virtual environments and install package using those environments.

Start Anaconda by launching "Anaconda Prompt"

Typing "conda info --envs" will show you all available environments. Normally you will only see one : "root".

Create a new environment by typing :

"conda create --name envName python=3.5" and type y when prompt.

Copy the installation path (in this example : C:\Users\jdg2018\AppData\Local\conda\conda\envs\envName)

if you type again "conda info --envs" you should see your newly created environment.

Activate your new environment by typing "activate envName". You should see (envName) in front of your command line.

You can now install any library you need by using pip as you would normally do. 

For example : "pip install see".

You can list what packages are installed using "conda list".

For more information on how to use Anaconda : https://conda.io/docs/user-guide/getting-started.html

If you want to use this environment inside Pycharm you have to click on "File->Settings->Project->Project Interpreter
-> the grey gear->Add local" and then copy the path where you install your environment earlier in front of \python.exe. 

Then press ok and apply. Now the interpreter should be using your environment.

Remember to give a clear readme and to list all the libraries you used to facilitate the corrector life. Make sure he can easily run your different projects. 
A happy corrector is one that will give more points ;).
