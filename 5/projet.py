from flask import Flask
import itertools
import json

app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def hello_world():
	#input = request.form['input'];
	
	input = "1  3 4"
	numbers = input.split(' ')
 
    numbers = [1, 2, 3]
    retour = []

    for L in range(0, len(numbers)+1):
        for subset in itertools.combinations(numbers, L):
			#if len(subset) is not 0:
			retour.append(" ".join(subset))
			
    return json.dumps({"result": retour})